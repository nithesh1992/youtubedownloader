package sample;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

/**
 * Created by Nithesh on 2/27/2016.
 */
public class GetVideo {


    public static void download(String url) throws IOException {


        ProcessBuilder builder = new ProcessBuilder(
                "cmd.exe", "/c", "youtube-dl.exe", url);


        builder.redirectErrorStream(true);
        Process p = builder.start();
        BufferedReader r = new BufferedReader(new InputStreamReader(p.getInputStream()));
        String line;
        while (true)
        {
            line = r.readLine();

            if (line == null) {
                break;
            }

        }

    }

}
