package sample;

import javafx.animation.KeyFrame;
import javafx.animation.Timeline;
import javafx.application.Platform;
import javafx.concurrent.Task;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.ProgressBar;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextField;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.URL;
import java.util.ResourceBundle;

public class Controller implements Initializable {


    String video_link;
    String result;

    @FXML
    private Button download_btn;
    public TextField url_field;
    public static TextArea output;
    public static ProgressBar download_status;




    @Override
    public void initialize(URL location, ResourceBundle resources)
    {


        download_btn.setOnAction(e -> {
            try {
               // output.setDisable(true);
                video_link = url_field.getText();

                if(video_link.isEmpty())
                {
                    AlertBox.display("No Link Entered", "Error: Enter a Valid URL");
                }

                download(video_link);


            } catch (IOException e1) {
                e1.printStackTrace();
            }
        });
    }

    public static void download (String video_link) throws IOException{
        String audio_config = " --extract-audio --audio-format=wav --audio-quality=0 " ;
        StringBuilder command = new StringBuilder();
        command.append("youtube-dl.exe");
        command.append(audio_config);
       // command.append("--all-subs");
        String commands = command.toString();
        ProcessBuilder builder = new ProcessBuilder(
                "cmd.exe", "/c", commands, video_link);

        builder.redirectErrorStream(true);
        Process p = builder.start();
        BufferedReader r = new BufferedReader(new InputStreamReader(p.getInputStream()));
        String line;



        while (true)
        {
            line = r.readLine();
            System.out.println(line);

            if(line.contains("ERROR"))
            {
                AlertBox.display("Message:" , "Error! Please Try Again");
                break;
            }
            if (line == null) {
                AlertBox.display("Message:" , "Download Completed");
                break;
            }

        }


        // test url :   https://www.youtube.com/watch?v=TsqbxwQLeEE
        //download_status.setProgress(1.0);
    }

    protected static void setProgress(String value)
    {
        //download_status.setProgress(value);
        //output.setText(value);
    }
}
